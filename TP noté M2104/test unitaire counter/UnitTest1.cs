﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TP_noté_M2104;

namespace test_unitaire_counter
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestInstentiation()
        {
            counter counter = new counter();
            Assert.IsTrue(counter.get_value() == 0);
        }

        [TestMethod]
        public void TestDecrement0()
        {
            counter counter = new counter();
            counter.decrement();
            Assert.IsTrue(counter.get_value() == 0);
        }

        [TestMethod]
        public void TestIncrement()
        {
            counter counter = new counter();
            counter.increment();
            Assert.IsTrue(counter.get_value() == 1);
        }

        [TestMethod]
        public void TestDecrementNormal()
        {
            counter counter = new counter();
            counter.increment();
            counter.increment();
            counter.decrement();
            Assert.IsTrue(counter.get_value() == 1);
        }

        [TestMethod]
        public void TestRAZ()
        {
            counter counter = new counter();
            counter.increment();
            counter.increment();
            counter.increment();
            counter.increment();
            counter.increment();
            counter.RAZ();
            Assert.IsTrue(counter.get_value() == 0);
        }
    }
}
