﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TP_noté_M2104
{
    public partial class Form1 : Form
    {
        counter counter = new counter();
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.counter.increment();
            this.label1.Text = this.counter.get_value().ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.counter.decrement();
            this.label1.Text = this.counter.get_value().ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.counter.RAZ();
            this.label1.Text = this.counter.get_value().ToString();
        }
    }
}
