﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP_noté_M2104
{
    public class counter
    {
        private int value;

        public counter()
        {
            this.value = 0;
        }

        public void increment()
        {
            this.value++;
        }

        public void decrement()
        {
            this.value--;
            if (this.value < 0)
                this.value = 0;
        }

        public void RAZ()
        {
            this.value = 0;
        }

        public int get_value()
        {
            return this.value;
        }
    }
}
